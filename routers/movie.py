from fastapi import Path, Query, Depends, APIRouter
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder
from typing import List
from config.database import session
from models.movie import Movie as MovieModel
from middlewares.jwt_bearer import JWTBearer
from services.movie import MovieService
from schemas.movie import Movie

movie_router = APIRouter()

## Métodos HTTP en FastAPI:
"""
Los principales métodos soportados por HTTP y por ello usados por una API REST son:
    POST: crear un recurso nuevo.
    PUT: modificar un recurso existente.
    GET: consultar información de un recurso.
    DELETE: eliminar un recurso.
"""

## Tipos de respuesta:
    # Importa la clase de fastapi.response JSONResponse
@movie_router.get('/movies', tags = ['Movies'], response_model = List[Movie], status_code = 200, dependencies = [Depends(JWTBearer())])
def get_movies() -> List[Movie]:
    db = session()
    result = MovieService(db).get_movies()
    return JSONResponse(status_code = 200, content = jsonable_encoder(result))

## Validaciones de parámetros:
    # Importa las clases de fastapi Path & Query
## Parámetros de ruta:
@movie_router.get('/movies/{id}', tags = ['Movies'], response_model = Movie)
def get_movie(id:int = Path(ge = 1, le = 2000)) -> Movie:
    db = session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(status_code = 404, content = {'Mensaje':'No encontrado'})
    return JSONResponse(status_code = 200, content = jsonable_encoder(result))

## Parámetros query:
@movie_router.get('/movies/', tags = ['Movies'], response_model = List[Movie])
def get_movies_by_category(category:str = Query(min_length = 5, max_length = 15)) -> List[Movie]:
    db = session()
    result = MovieService(db).get_movies_by_category(category)
    return JSONResponse(content = jsonable_encoder(result))

## Método POST:
## Creación de esquemas:
@movie_router.post('/movies', tags = ['Movies'], response_model = dict, status_code = 201)
def create_movie(movie:Movie) -> dict:
    db = session()
    MovieService(db).create_movie(movie)
    return JSONResponse(status_code = 201, content = {'mensaje':'se registró la película'})

## Método PUT (modificar datos):
@movie_router.put('/movies/{id}', tags = ['Movies'], response_model = dict, status_code = 200)
def update_movie(id:int, movie:Movie) -> dict:
    db = session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(status_code = 404, content = {'Mensaje':'No encontrado'})
    MovieService(db).update_movie(id, movie)
    return JSONResponse(status_code = 200, content = {'mensaje':'se modificó la película'})

## Método DELETE:
@movie_router.delete('/movies/{id}', tags = ['Movies'], response_model = dict, status_code = 200)
def delete_movie(id:int) -> dict:
    db = session()
    result: MovieModel = db.query(MovieModel).filter(MovieModel.id == id).first()
    if not result:
        return JSONResponse(status_code = 404, content = {'Mensaje':'No encontrado'})
    MovieService(db).delete_movie(id)
    return JSONResponse(status_code = 200, content = {'mensaje':'se eliminó la película'})