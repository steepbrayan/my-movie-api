from utils.jwt_manager import validate_token
from fastapi.security import HTTPBearer
from fastapi import Request, HTTPException

class JWTBearer(HTTPBearer):
    async def __call__(self, request:Request):
        auth = await super().__call__(request)
        data = validate_token(auth.credentials)
        if data['email'] != 'admin@myapp.com':
            raise HTTPException(status_code=403, details='Las credenciales son inválidas')