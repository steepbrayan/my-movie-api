from pydantic import BaseModel, Field
from typing import Optional

## Validaciones de tipos de datos:
    # Modifica la clase con la clase de pydantic Field
class Movie(BaseModel):
    id: Optional[int] = None
    title:str = Field(min_length = 5, max_length = 100)
    overview:str = Field(min_length = 15, max_length = 50)
    year:int = Field(le = 2022)
    rating:float = Field(ge = 1, le = 10)
    category:str = Field(min_length = 5, max_length = 15)

    class Config:
        schema_extra = {
            'example':{
                'id':1,
                'title':'Mi película',
                'overview':'Descripción de mi película',
                'year':2022,
                'rating':6.9,
                'category':'Ficción'
            }
        }
