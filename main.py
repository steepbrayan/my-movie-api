from fastapi import FastAPI
from config.database import engine, base
from middlewares.error_handler import ErrorHandler
from routers.movie import movie_router
from routers.user import user_router

## APP:
app = FastAPI()                             # Instancia de FASTAPI
app.title = "Mi aplicación con FastAPI"     # Título expuesto
app.version = '0.0.1'                       # Versión de la API
app.add_middleware(ErrorHandler)
app.include_router(movie_router)
app.include_router(user_router)

base.metadata.create_all(bind = engine)

movies = [
    {
        'id': 1,
        'title': 'Avatar',
        'overview': "En un exuberante planeta llamado Pandora viven los Na'vi, seres que ...",
        'year': '2009',
        'rating': 7.8,
        'category': 'Acción'    
    },
    {
        'id': 2,
        'title': 'Avatar',
        'overview': "En un exuberante planeta llamado Pandora viven los Na'vi, seres que ...",
        'year': '2009',
        'rating': 7.8,
        'category': 'Acción'    
    }
]

## Métodos HTTP en FastAPI:
"""
Los principales métodos soportados por HTTP y por ello usados por una API REST son:
    POST: crear un recurso nuevo.
    PUT: modificar un recurso existente.
    GET: consultar información de un recurso.
    DELETE: eliminar un recurso.
"""